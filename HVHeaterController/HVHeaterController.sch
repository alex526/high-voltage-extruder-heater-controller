EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SamacSys_Parts:4N25 Q?
U 1 1 5EE880CF
P 3050 3400
F 0 "Q?" H 3800 3665 50  0000 C CNN
F 1 "4N25" H 3800 3574 50  0000 C CNN
F 2 "DIP762W60P254L730H600Q6N" H 4400 3500 50  0001 L CNN
F 3 "http://optoelectronics.liteon.com/upload/download/DS-70-99-0010/S_110_4N25(26).pdf" H 4400 3400 50  0001 L CNN
F 4 "Transistor Output Optocouplers PTR 20%, 2.5KV" H 4400 3300 50  0001 L CNN "Description"
F 5 "6" H 4400 3200 50  0001 L CNN "Height"
F 6 "859-4N25" H 4400 3100 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Lite-On/4N25?qs=gnaPJ2cis70RM9udB%2FP83w%3D%3D" H 4400 3000 50  0001 L CNN "Mouser Price/Stock"
F 8 "Lite-On" H 4400 2900 50  0001 L CNN "Manufacturer_Name"
F 9 "4N25" H 4400 2800 50  0001 L CNN "Manufacturer_Part_Number"
	1    3050 3400
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:IPN80R750P7ATMA1 Q?
U 1 1 5EE892B5
P 5500 2950
F 0 "Q?" H 5900 3215 50  0000 C CNN
F 1 "IPN80R750P7ATMA1" H 5900 3124 50  0000 C CNN
F 2 "IPN80R750P7ATMA1" H 6150 3050 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/IPN80R750P7ATMA1.pdf" H 6150 2950 50  0001 L CNN
F 4 "MOSFET LOW POWER_NEW" H 6150 2850 50  0001 L CNN "Description"
F 5 "1.8" H 6150 2750 50  0001 L CNN "Height"
F 6 "726-IPN80R750P7ATMA1" H 6150 2650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=726-IPN80R750P7ATMA1" H 6150 2550 50  0001 L CNN "Mouser Price/Stock"
F 8 "Infineon" H 6150 2450 50  0001 L CNN "Manufacturer_Name"
F 9 "IPN80R750P7ATMA1" H 6150 2350 50  0001 L CNN "Manufacturer_Part_Number"
	1    5500 2950
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:DF06S-G BR?
U 1 1 5EE89FA8
P 2700 1300
F 0 "BR?" H 3150 1565 50  0000 C CNN
F 1 "DF06S-G" H 3150 1474 50  0000 C CNN
F 2 "SOP510P1015X260-4N" H 3450 1400 50  0001 L CNN
F 3 "" H 3450 1300 50  0001 L CNN
F 4 "Bridge Rectifiers DFS GPP 1A 600V Rect. Bridge Diode" H 3450 1200 50  0001 L CNN "Description"
F 5 "2.6" H 3450 1100 50  0001 L CNN "Height"
F 6 "750-DF06S-G" H 3450 1000 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Comchip-Technology/DF06S-G?qs=tw%252BuQ%2FB6PO3wJeCDrmaAog%3D%3D" H 3450 900 50  0001 L CNN "Mouser Price/Stock"
F 8 "Comchip Technology" H 3450 800 50  0001 L CNN "Manufacturer_Name"
F 9 "DF06S-G" H 3450 700 50  0001 L CNN "Manufacturer_Part_Number"
	1    2700 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5EE8A876
P 4850 5400
F 0 "#PWR?" H 4850 5150 50  0001 C CNN
F 1 "GND" H 4855 5227 50  0000 C CNN
F 2 "" H 4850 5400 50  0001 C CNN
F 3 "" H 4850 5400 50  0001 C CNN
	1    4850 5400
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:BZV55-C12,135 Z?
U 1 1 5EE8ADDD
P 4450 1900
F 0 "Z?" H 4750 2165 50  0000 C CNN
F 1 "BZV55-C12,135" H 4750 2074 50  0000 C CNN
F 2 "SOD-80C" H 4850 2050 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BZV55_SER.pdf" H 4850 1950 50  0001 L CNN
F 4 "NEXPERIA - BZV55-C12,135 - DIODE, ZENER, 12V, 500MW, SOD-80C-2" H 4850 1850 50  0001 L CNN "Description"
F 5 "" H 4850 1750 50  0001 L CNN "Height"
F 6 "771-BZV55-C12135" H 4850 1650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Nexperia/BZV55-C12135?qs=me8TqzrmIYUxHTnJu%2FkyEw%3D%3D" H 4850 1550 50  0001 L CNN "Mouser Price/Stock"
F 8 "Nexperia" H 4850 1450 50  0001 L CNN "Manufacturer_Name"
F 9 "BZV55-C12,135" H 4850 1350 50  0001 L CNN "Manufacturer_Part_Number"
	1    4450 1900
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5EE8CE20
P 4450 1600
F 0 "R?" H 4520 1646 50  0000 L CNN
F 1 "15K" H 4520 1555 50  0000 L CNN
F 2 "" V 4380 1600 50  0001 C CNN
F 3 "~" H 4450 1600 50  0001 C CNN
	1    4450 1600
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5EE8D59A
P 3900 2500
F 0 "R?" H 3970 2546 50  0000 L CNN
F 1 "R" H 3970 2455 50  0000 L CNN
F 2 "" V 3830 2500 50  0001 C CNN
F 3 "~" H 3900 2500 50  0001 C CNN
	1    3900 2500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
