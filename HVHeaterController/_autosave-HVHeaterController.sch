EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L SamacSys_Parts:IPN80R750P7ATMA1 Q2
U 1 1 5EE892B5
P 5400 2950
F 0 "Q2" H 5800 3215 50  0000 C CNN
F 1 "IPN80R750P7ATMA1" H 5800 3124 50  0000 C CNN
F 2 "SamacSys_Parts:IPN80R750P7ATMA1" H 6050 3050 50  0001 L CNN
F 3 "https://componentsearchengine.com/Datasheets/1/IPN80R750P7ATMA1.pdf" H 6050 2950 50  0001 L CNN
F 4 "MOSFET LOW POWER_NEW" H 6050 2850 50  0001 L CNN "Description"
F 5 "1.8" H 6050 2750 50  0001 L CNN "Height"
F 6 "726-IPN80R750P7ATMA1" H 6050 2650 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.com/Search/Refine.aspx?Keyword=726-IPN80R750P7ATMA1" H 6050 2550 50  0001 L CNN "Mouser Price/Stock"
F 8 "Infineon" H 6050 2450 50  0001 L CNN "Manufacturer_Name"
F 9 "IPN80R750P7ATMA1" H 6050 2350 50  0001 L CNN "Manufacturer_Part_Number"
	1    5400 2950
	1    0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:DF06S-G BR1
U 1 1 5EE89FA8
P 3050 1250
F 0 "BR1" H 3500 1515 50  0000 C CNN
F 1 "DF06S-G" H 3500 1424 50  0000 C CNN
F 2 "SamacSys_Parts:SOP510P1015X260-4N" H 3800 1350 50  0001 L CNN
F 3 "" H 3800 1250 50  0001 L CNN
F 4 "Bridge Rectifiers DFS GPP 1A 600V Rect. Bridge Diode" H 3800 1150 50  0001 L CNN "Description"
F 5 "2.6" H 3800 1050 50  0001 L CNN "Height"
F 6 "750-DF06S-G" H 3800 950 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Comchip-Technology/DF06S-G?qs=tw%252BuQ%2FB6PO3wJeCDrmaAog%3D%3D" H 3800 850 50  0001 L CNN "Mouser Price/Stock"
F 8 "Comchip Technology" H 3800 750 50  0001 L CNN "Manufacturer_Name"
F 9 "DF06S-G" H 3800 650 50  0001 L CNN "Manufacturer_Part_Number"
	1    3050 1250
	1    0    0    1   
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5EE8A876
P 4500 2150
F 0 "#PWR0101" H 4500 1900 50  0001 C CNN
F 1 "GND" H 4505 1977 50  0000 C CNN
F 2 "" H 4500 2150 50  0001 C CNN
F 3 "" H 4500 2150 50  0001 C CNN
	1    4500 2150
	-1   0    0    -1  
$EndComp
$Comp
L SamacSys_Parts:BZV55-C12,135 Z1
U 1 1 5EE8ADDD
P 5100 2050
F 0 "Z1" H 5400 2315 50  0000 C CNN
F 1 "BZV55-C12,135" H 5400 2224 50  0000 C CNN
F 2 "SamacSys_Parts:SOD-80C" H 5500 2200 50  0001 L CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/BZV55_SER.pdf" H 5500 2100 50  0001 L CNN
F 4 "NEXPERIA - BZV55-C12,135 - DIODE, ZENER, 12V, 500MW, SOD-80C-2" H 5500 2000 50  0001 L CNN "Description"
F 5 "" H 5500 1900 50  0001 L CNN "Height"
F 6 "771-BZV55-C12135" H 5500 1800 50  0001 L CNN "Mouser Part Number"
F 7 "https://www.mouser.co.uk/ProductDetail/Nexperia/BZV55-C12135?qs=me8TqzrmIYUxHTnJu%2FkyEw%3D%3D" H 5500 1700 50  0001 L CNN "Mouser Price/Stock"
F 8 "Nexperia" H 5500 1600 50  0001 L CNN "Manufacturer_Name"
F 9 "BZV55-C12,135" H 5500 1500 50  0001 L CNN "Manufacturer_Part_Number"
	1    5100 2050
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5EE8CE20
P 5100 1300
F 0 "R2" H 5170 1346 50  0000 L CNN
F 1 "15K" H 5170 1255 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 1300 50  0001 C CNN
F 3 "~" H 5100 1300 50  0001 C CNN
	1    5100 1300
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 5EE8D59A
P 5100 3100
F 0 "R3" H 5170 3146 50  0000 L CNN
F 1 "15K" H 5170 3055 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 5030 3100 50  0001 C CNN
F 3 "~" H 5100 3100 50  0001 C CNN
	1    5100 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5EE90A78
P 4950 1700
F 0 "C1" V 4698 1700 50  0000 C CNN
F 1 "C" V 4789 1700 50  0000 C CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4988 1550 50  0001 C CNN
F 3 "~" H 4950 1700 50  0001 C CNN
	1    4950 1700
	0    -1   1    0   
$EndComp
Wire Wire Line
	5100 2050 5100 1700
Wire Wire Line
	4800 1700 4500 1700
Wire Wire Line
	4500 1700 4500 2050
Wire Wire Line
	4500 2050 4500 2150
Connection ~ 4500 2050
Wire Wire Line
	5100 1450 5100 1700
Connection ~ 5100 1700
Wire Wire Line
	3950 1250 4500 1250
Wire Wire Line
	4500 1250 4500 1700
Connection ~ 4500 1700
Wire Wire Line
	5100 1150 3950 1150
$Comp
L power:GND #PWR0102
U 1 1 5EEA3FA2
P 5400 3400
F 0 "#PWR0102" H 5400 3150 50  0001 C CNN
F 1 "GND" H 5405 3227 50  0000 C CNN
F 2 "" H 5400 3400 50  0001 C CNN
F 3 "" H 5400 3400 50  0001 C CNN
	1    5400 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 1150 6500 1150
Connection ~ 5100 1150
Wire Wire Line
	5400 3050 5400 3250
Wire Wire Line
	5400 2950 5100 2950
Wire Wire Line
	5100 3250 5400 3250
Connection ~ 5400 3250
Wire Wire Line
	5400 3250 5400 3400
Wire Wire Line
	4500 2600 5100 2600
Wire Wire Line
	5100 2600 5100 2050
Connection ~ 5100 2050
Wire Wire Line
	5100 2700 5100 2950
Connection ~ 5100 2950
Wire Wire Line
	6500 1250 6300 1250
Wire Wire Line
	6300 2950 6200 2950
$Comp
L Device:R R1
U 1 1 5EEABB2D
P 2750 2500
F 0 "R1" V 2543 2500 50  0000 C CNN
F 1 "180" V 2634 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" V 2680 2500 50  0001 C CNN
F 3 "~" H 2750 2500 50  0001 C CNN
	1    2750 2500
	0    1    1    0   
$EndComp
$Comp
L Connector:Screw_Terminal_01x02 J3
U 1 1 5EEAD368
P 6700 1150
F 0 "J3" H 6780 1142 50  0000 L CNN
F 1 "Screw_Terminal_01x02" H 6780 1051 50  0000 L CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43045-0210_2x01-1MP_P3.00mm_Horizontal" H 6700 1150 50  0001 C CNN
F 3 "~" H 6700 1150 50  0001 C CNN
	1    6700 1150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J2
U 1 1 5EEADD58
P 2500 1150
F 0 "J2" H 2608 1331 50  0000 C CNN
F 1 "Conn_01x02_Male" H 2608 1240 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43045-0210_2x01-1MP_P3.00mm_Horizontal" H 2500 1150 50  0001 C CNN
F 3 "~" H 2500 1150 50  0001 C CNN
	1    2500 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 1150 3050 1150
Wire Wire Line
	3050 1250 2700 1250
Wire Wire Line
	6300 1250 6300 2950
$Comp
L Isolator:TLP291 U1
U 1 1 5EEC30F8
P 3400 2600
F 0 "U1" H 3400 2925 50  0000 C CNN
F 1 "TLP291" H 3400 2834 50  0000 C CNN
F 2 "Package_SO:SOIC-4_4.55x2.6mm_P1.27mm" H 3200 2400 50  0001 L CIN
F 3 "https://toshiba.semicon-storage.com/info/docget.jsp?did=12884&prodName=TLP291" H 3400 2600 50  0001 L CNN
	1    3400 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 2500 3100 2500
Wire Wire Line
	3100 2600 3100 2700
Wire Wire Line
	3700 2500 4500 2500
Wire Wire Line
	4500 2500 4500 2600
Wire Wire Line
	3700 2700 5100 2700
Wire Wire Line
	2400 2600 3100 2600
Wire Wire Line
	2400 2500 2600 2500
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5EEAF850
P 2200 2500
F 0 "J1" H 2308 2681 50  0000 C CNN
F 1 "Conn_01x02_Male" H 2308 2590 50  0000 C CNN
F 2 "SamacSys_Parts:15913024" H 2200 2500 50  0001 C CNN
F 3 "~" H 2200 2500 50  0001 C CNN
	1    2200 2500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
